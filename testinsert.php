<?php
//Include php file to connect to database
include("sqlconnection.php");

//After extract data from email
//Insert inventory update record into database
$invUpd = "INSERT INTO inv_update (S_ID, IU_Date, IU_Time, IU_TotalAmount)
VALUES (1, '2022-03-17', '09:06:00', 4500);";
mysqli_query($connection, $invUpd) or die(mysqli_error($connection));

//Get the current inventory update ID
$getIUID = "SELECT MAX(IU_ID) FROM inv_update";
$iuResult = mysqli_query($connection, $getIUID);
while($iuRow = mysqli_fetch_assoc($iuResult)){
    $newIU = $iuRow['P_Quantity'] + 1;
}

//read file loop
for($i = 0; $i <= $noOfInv; $i++){
    //Check if product exists
    $prodRetrieve = "SELECT P_Barcode, P_ID, P_Quantity from product where P_Barcode = '$Barcode';";
    $prodResult = mysqli_query($connection, $prodRetrieve);
    //If product exists
    if(mysqli_num_rows($prodResult) > 0){
        //Fetch current quantity and add quantity + current product ID
        while($prodRow = mysqli_fetch_assoc($prodResult)){
            $newQuantity = $prodRow['P_Quantity'] + $Quantity;
            $productID = $prodRow['P_ID'];
        }

        //Update quantity
        $prodUpdate = "UPDATE product SET P_Quantity = '$newQuantity' where P_Barcode = '$Barcode';";
        $prodUResult = mysqli_query($connection, $prodUpdate)  or die(mysqli_error($connection));

        //Add record to iu_record table
        $prodAdd = "INSERT INTO iu_record (P_ID, IU_ID, IU_Quantity, IU_Remarks, Supp_Price)
        VALUES ('$productID', '$newIU', '$Quantity', '$Remarks', '$Price');";
        mysqli_query($connection, $prodAdd) or die(mysqli_error($connection));
    }else{
        //Add new product to table
        $prodAdd = "INSERT INTO product (P_Name, P_Barcode, P_Quantity, P_Price)
        VALUES ('$P_Name', '$P_Barcode', '$P_Quantity', '$P_Price');";
        mysqli_query($connection, $prodAdd) or die(mysqli_error($connection));

        //Get the current inventory update ID
        $getProID = "SELECT MAX(P_ID) FROM product";
        $proResult = mysqli_query($connection, $getProID);
        while($proRow = mysqli_fetch_assoc($proResult)){
            $newPro = $proRow['P_ID'] + 1;
        }

        //Add record to iu_record table
        $prodAdd = "INSERT INTO iu_record (P_ID, IU_ID, IU_Quantity, IU_Remarks, Supp_Price)
        VALUES ('$newPro', '$newIU', '$Quantity', '$Remarks', '$Price');";
        mysqli_query($connection, $prodAdd) or die(mysqli_error($connection));
    }


}

//Insert update result
$resultUpd = "INSERT INTO iu_result
VALUES ($newIU, '1', '$remarks');";
mysqli_query($connection, $resultUpd) or die(mysqli_error($connection));

?>