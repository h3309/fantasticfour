<?php
    //Initialize the session
    session_start();
?>

<html>
<title>Report View</title>
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.10.2/dist/umd/popper.min.js" integrity="sha384-7+zCNj/IqJ95wo16oMtfsKbZ9ccEh31eOz1HGyDuCQ6wgnyJNSYdrPa03rtR1zdB" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.min.js" integrity="sha384-QJHtvGhmr9XOIpI6YVutG+2QOK9T+ZnN4kzFN1RtK3zEFEIsxhlmWl5/YESvpZ13" crossorigin="anonymous"></script>

    <title>Report View</title>
    <style>
        .a4: {
            padding: 30px;
        }
    </style>
</head>

<?php
    //Include php file to connect to database
    include("sqlconnection.php");

    //Get report details
    $getRecordList = "SELECT iu.IU_ID, s.S_Name, s.S_ID, s.S_Email, iu.IU_Date, iu.IU_Time, iu_TotalAmount FROM inv_update iu
    join supplier s on iu.S_ID = s.S_ID
    where iu.IU_ID = {$_SESSION['selectIU']}";
    //Fetch the query result
    $Result = mysqli_query($connection, $getRecordList);
    if(mysqli_num_rows($Result) > 0){
        while($row = mysqli_fetch_assoc($Result)){
            $Date = $row['IU_Date'];
            $Time = $row['IU_Time'];
            $SName = $row['S_Name'];
            $SID = $row['S_ID'];
            $SEmail = $row['S_Email'];
        }
    }
	
?>

<body>
    <div style="padding: 20px;">
        <div class="a4">
            <h2 style="text-align: center;">Inventory Update Report</h2>
            <p style="text-align: right;"><b>Report ID:</b> <?php echo $_SESSION['selectIU'];?> </p>
            <p style="text-align: left;"><b>Date:</b> <?php echo $Date; ?><br>
            <b>Time:</b> <?php echo $Time; ?></p>
            
            <h3>Supplier Detail:</h3>
            <p><b>Supplier Name/ID:</b> <?php echo $SName, "/", $SID?><br>
            <b>Email:</b> <?php echo $SEmail?> </p>

            <hr style='width:auto;'>

            <h3>Product Details:</h3>
            <?php
                $getDetails = "SELECT p.P_Name, p.P_ID, p.P_Barcode, m.M_Name, pc.PC_Name, iur.IU_Quantity, iur.Supp_Price, iur.IU_Remarks FROM iu_record iur
                join product p on iur.P_ID = p.P_ID
                join prod_cat pc on p.PC_ID = pc.PC_ID
                join manufacturer m on p.M_ID = m.M_ID
                WHERE iur.IU_ID = {$_SESSION['selectIU']}
                ORDER BY p.P_ID";
                //Fetch the query result
                $Result = mysqli_query($connection, $getDetails);
                if(mysqli_num_rows($Result) > 0){
                    $i = 1;
                    $total = 0;
                    echo "<table class='table table-striped table-bordered'>";
                    echo "<tr><th>#</th><th>Product ID</th><th>Barcode</th><th>Product Name</th><th>Manufacturer</th><th>Category</th><th>Quantity</th><th>Unit Price (RM)</th><th>Total Price (RM)</th><th>Remarks</th><tr>";
                    while($row = mysqli_fetch_assoc($Result)){
                        echo "<tr><td>", $i++, "</td>";
                        echo "<td>{$row['P_ID']}</td>";
                        echo "<td>{$row['P_Barcode']}</td>";
                        echo "<td>{$row['P_Name']}</td>";
                        echo "<td>{$row['M_Name']}</td>";
                        echo "<td>{$row['PC_Name']}</td>";
                        echo "<td>{$row['IU_Quantity']}</td>";
                        echo "<td style='text-align:right;'>{$row['Supp_Price']}</td>";
                        echo "<td style='text-align:right;'>", $row['IU_Quantity']*$row['Supp_Price'], "</td>";
                        echo "<td>{$row['IU_Remarks']}</td></tr>";
                        $total += $row['IU_Quantity']*$row['Supp_Price'];
                    }
                    echo "</table>";
                    echo "<p style='text-align:right;'>Amount in total: <strong>RM$total</strong></p>";
                }

            ?>
            <div id='buttondiv' style='float: right; padding: 10px;'>
                <button onClick="window.print()" class="btn btn-primary">Print report</button>
                <a href="report.php" class="btn btn-secondary">Back</button></a>
            </div>
        </div>
    </div>

</body>
</html>