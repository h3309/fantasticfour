<html>
	<head>
		<!-- Required meta tags -->
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">

		<!-- Bootstrap CSS -->
		<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
		<script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.10.2/dist/umd/popper.min.js" integrity="sha384-7+zCNj/IqJ95wo16oMtfsKbZ9ccEh31eOz1HGyDuCQ6wgnyJNSYdrPa03rtR1zdB" crossorigin="anonymous"></script>
		<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.min.js" integrity="sha384-QJHtvGhmr9XOIpI6YVutG+2QOK9T+ZnN4kzFN1RtK3zEFEIsxhlmWl5/YESvpZ13" crossorigin="anonymous"></script>

		<!--Link to external CSS configs-->
		<link rel = "stylesheet" type = "text/css" href = "style.css"/>
		<title>Inventory Management</title>
	</head>

	<?php
        function showInventory(){
            //Include php file to connect to database
            include("sqlconnection.php");

            //Query for product list
            $Query = "SELECT p.P_ID, p.P_Name, p.P_Barcode, p.P_Quantity, p.P_Price, s.S_Name, m.M_Name from product p
            join supplier s on p.S_ID = s.S_ID
            join manufacturer m on p.M_ID = m.M_ID
            ORDER BY p.P_ID";
            //Fetch the query result
            $Result = mysqli_query($connection, $Query);
            if(mysqli_num_rows($Result) > 0){
                //If there products in the inventory
                //Display the records in a table
                echo "<p><table class='table' style='width:100%;'>";
                echo "<tr><th>Product ID</th><th>Product Name</th><th>Product Barcode</th><th>Quantity</th><th>Price (RM)</th><th>Supplier</th><th>Manufacturer</th></tr>";
                while($row = mysqli_fetch_assoc($Result)){
                    echo "<tr><td>{$row['P_ID']}</td>";
                    echo "<td>{$row['P_Name']}</td>";
					echo "<td>{$row['P_Barcode']}</td>";
                    echo "<td>{$row['P_Quantity']}</td>";
                    echo "<td>{$row['P_Price']}</td>";
                    echo "<td>{$row['S_Name']}</td>";
                    echo "<td>{$row['M_Name']}</td>";
                }
                echo "</table></p>";
            }else{
                //If there are no records, display message
                echo"<p>No products in inventory!</p>";
            }
        }
        
        function searchInventory($searchTerm, $searchCat, $plow, $phigh, $pflag, $qlow, $qhigh, $qflag){
            //Include php file to connect to database
            include("sqlconnection.php");

            //Initialize search category variables for SQL query
            $name = "";
            $category = "";

            switch($searchCat){
                case "P_ID":
                    $name = "p";
                    $category = "Product ID";
                    break;
                case "P_Name": 
                    $name = "p";
                    $category = "Product Name";
                    break;
				case "P_Barcode": 
					$name = "p";
					$category = "Product Barcode";
					break;
                case "S_Name":
                    $name = "s";
                    $category = "Supplier";
                    break;
                case "M_Name":
                    $name = "m";
                    $category = "Manufacturer";
                    break;
            }

            //If user is using the price filter
            if($pflag == false){
                $price = "";
            }else{
				//Create price filter portion of the query
                $price = " AND (p.P_Price BETWEEN {$plow} AND {$phigh})";
			}

            //If user is using the quantity filter
            if($qflag == false){
                $quantity = "";
            }else{
				$quantity = " AND (p.P_Quantity BETWEEN {$qlow} AND {$qhigh})";
			}

            echo "<p>Searching from $category:</p>";
            //SQL statement to search from database
            $sqlSearch ="SELECT p.P_ID, p.P_Name, p.P_Barcode, p.P_Quantity, p.P_Price, s.S_Name, m.M_Name from product p
            join supplier s on p.S_ID = s.S_ID
            join manufacturer m on p.M_ID = m.M_ID
            where ({$name}.{$searchCat} like '%$searchTerm%')$price$quantity";
            
            //Query the database
            $result = mysqli_query($connection, $sqlSearch);
            //Fetch rows from the query
            $row = mysqli_fetch_row($result);
            
            if($row == NULL){
                //If no results, display no results found
                echo "<p>No results found</p>";
            }else{
                //else display the available search results
                echo "<table class='table' style='width:100%;'>";
                echo "<tr><th>Product ID</th><th>Product Name</th><th>Product Barcode</th><th>Quantity</th><th>Price (RM)</th><th>Supplier</th><th>Manufacturer</th></tr>";
                //Do while loop to keep fetching row values until the next row has nothing
                do {
                    echo "<tr><td>{$row[0]}</td>";
                    echo "<td>{$row[1]}</td>";
                    echo "<td>{$row[2]}</td>";
                    echo "<td>{$row[3]}</td>";
                    echo "<td>{$row[4]}</td>";
                    echo "<td>{$row[5]}</td>";
					echo "<td>{$row[6]}</td>";
                    $row = mysqli_fetch_row($result);
                } while ($row);
            }
        }
    ?>

	<body>
		<!--MenuBar-->
		<div>
			<nav class="navbar navbar-expand-lg navbar-light bg-light" style="padding:0px;border-top-left-radius:5px;" >
				<div class="container-fluid" >
					<div class="d-flex justify-content-start">
						<div class="d-flex justify-content-start" >
							<div id="d-logo" ondrop="drop(event)" ondragover="allowDrop(event)" >
								<img src="Logo.png" draggable="true" id="drag2" ondragstart="drag(event)" width="100" height="80"/>
							</div>
							<h1 style="margin:25px 0px 0px -40px; font-weight:bold;">Company_Name</h1>			
						</div>
					</div>

					<div class="d-flex justify-content-end " Style="margin-right:30px;">
						<button class="navbar-toggler" fill="#fff"  type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
							<span class="navbar-toggler-icon" ></span>
						</button>
						
						<div class="collapse navbar-collapse " id="navbarSupportedContent" >
							<ul class="navbar-nav me-auto mb-2 mb-lg-0" style="padding-left:10px; ">
								<li class="nav-item">
									<a class="nav-link" style="color:#fff;" aria-current="page" href="http://localhost/Hack2Hire/index.php">Dashboard</a>
								</li>
								
								<li class="nav-item-active">
									<a class="nav-link active" style="color:#fff;" href="http://localhost/Hack2Hire/stock.php">Stock Management</a>
								</li>
								
								<li class="nav-item">
									<a class="nav-link" style="color:#fff;" href="http://localhost/Hack2Hire/report.php">Report</a>
								</li>
							</ul>
						</div>
					</div>	
				</div>		  
			</nav>
		<div>
			
		<!--ContentStart-->
		<div>
			<?php if(isset($_GET['error'])){  ?>
				<div class="alert alert-warning d-flex align-items-right" role="alert" style="text-align:right;">
					<svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" fill="currentColor" class="bi bi-exclamation-triangle" viewBox="0 0 16 16">
						<path d="M7.938 2.016A.13.13 0 0 1 8.002 2a.13.13 0 0 1 .063.016.146.146 0 0 1 .054.057l6.857 11.667c.036.06.035.124.002.183a.163.163 0 0 1-.054.06.116.116 0 0 1-.066.017H1.146a.115.115 0 0 1-.066-.017.163.163 0 0 1-.054-.06.176.176 0 0 1 .002-.183L7.884 2.073a.147.147 0 0 1 .054-.057zm1.044-.45a1.13 1.13 0 0 0-1.96 0L.165 13.233c-.457.778.091 1.767.98 1.767h13.713c.889 0 1.438-.99.98-1.767L8.982 1.566z"/>
						<path d="M7.002 12a1 1 0 1 1 2 0 1 1 0 0 1-2 0zM7.1 5.995a.905.905 0 1 1 1.8 0l-.35 3.507a.552.552 0 0 1-1.1 0L7.1 5.995z"/>
					</svg>
					<div style="margin: 0px 5px 0px 5px;text-align:right;">
						To show the restock notification!!!	<?php echo $_GET['error'];  ?>		
					</div>
					<svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" fill="currentColor" class="bi bi-x-circle" viewBox="0 0 16 16">
						<path d="M8 15A7 7 0 1 1 8 1a7 7 0 0 1 0 14zm0 1A8 8 0 1 0 8 0a8 8 0 0 0 0 16z"/>
						<path d="M4.646 4.646a.5.5 0 0 1 .708 0L8 7.293l2.646-2.647a.5.5 0 0 1 .708.708L8.707 8l2.647 2.646a.5.5 0 0 1-.708.708L8 8.707l-2.646 2.647a.5.5 0 0 1-.708-.708L7.293 8 4.646 5.354a.5.5 0 0 1 0-.708z"/>
					</svg>
				</div>
			<?php } ?>	
		</div>
		

		<div style="margin:30px 30px 30px 30px;">
			<h2>Inventory List</h2>
			<div class="invcontent">
				<div class="searchsec">
					<!--Inventory search function-->
					<p style="color: black;">Inventory search:</p>
					<form action="" method="post">
						<input type="text" name="searchTerm" required>
						<div class="catsubmit">
							<!--Select function that allows the user to search for a specific category-->
							<select name="searchCat" id="searchCat">
								<option value="P_ID">Product ID</option>
								<option value="P_Name">Product Name</option>
								<option value="P_Barcode">Product Barcode</option>
								<option value="S_Name">Supplier</option>
								<option value="M_Name">Manufacturer</option>
							</select>
							<hr style='width:auto;'>
							<!--Section for additional filters like price and quantity-->
							Additional filters:
							<div>
								<table>
									<tr>
										<td><label><input type="checkbox" name="price" value="price">Price range:</label></td>
										<td><input type="text" name="plow" size=5> to <input type="text" name="phigh" size=5></td>
									</tr>
									<tr>
										<td><label><input type="checkbox" name="quantity" value="quantity">Quantity:</label></td>
										<td><input type="text" name="qlow" size=5> to <input type="text" name="qhigh" size=5></td>
									</tr>
								</table>
							</div>
							<p><input type="submit" name="search" value="Search"></p>
						</div>
					</form>
				</div>

				<div class="container-sm" style="background-color: rgba(0,0,0,0.2);border-radius:10px; item-align:center; padding: 10px 10px 10px 10px; box-shadow: 0 3px 10px rgb(0 0 0 / 0.2);">
					<!--To show everything in table --> 
					<?php
						//If the submit button with the "search" value is clicked
						if(isset($_POST['search'])){
							//Store the search term and category in session
							$searchTerm = $_POST['searchTerm'];
							$searchCat = $_POST['searchCat'];

							//If price filter is selected
							if(isset($_POST['price'])){
								//Store low and high in session
								$plow = $_POST['plow'];
								$phigh = $_POST['phigh'];
								$pflag = true;
							}else{
								$plow = "";
								$phigh = "";
								$pflag = false;
							}
							//If quantity filter is selected
							if(isset($_POST['quantity'])){
								//Store low and high in session
								$qlow = $_POST['qlow'];
								$qhigh = $_POST['qhigh'];
								$qflag = true;
							}else{
								$qlow = "";
								$qhigh = "";
								$qflag = false;
							}

							//Call inventory search function
							searchInventory($searchTerm, $searchCat, $plow, $phigh, $pflag, $qlow, $qhigh, $qflag);
						}else{
							//Call show inventory function
							showInventory();
						}
					?>
				</div>
			</div> 
		</div>
	</body>
</html>
