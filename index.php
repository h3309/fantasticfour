<html>
<title>Dashboard</title>
<head>
 <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

<!-- Bootstrap CSS -->
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
<script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.10.2/dist/umd/popper.min.js" integrity="sha384-7+zCNj/IqJ95wo16oMtfsKbZ9ccEh31eOz1HGyDuCQ6wgnyJNSYdrPa03rtR1zdB" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.min.js" integrity="sha384-QJHtvGhmr9XOIpI6YVutG+2QOK9T+ZnN4kzFN1RtK3zEFEIsxhlmWl5/YESvpZ13" crossorigin="anonymous"></script>

<!--Link to external CSS configs-->
	<link rel = "stylesheet" type = "text/css" href = "style.css"/>

</head>


<form>
	<!--MenuBar-->
	<div>
		<nav class="navbar navbar-expand-lg navbar-light bg-light" style="padding:0px;border-top-left-radius:5px;" >
		  <div class="container-fluid" >
		  <div class="d-flex justify-content-start">
			<div class="d-flex justify-content-start" >
				<div id="d-logo" ondrop="drop(event)" ondragover="allowDrop(event)" >
					<img src="Logo.png" draggable="true" id="drag2" ondragstart="drag(event)" width="100" height="80"/>
				</div>
					<h1 style="margin:25px 0px 0px -40px; font-weight:bold;">Company_Name</h1>						
				</div>
			</div>
			
			
			<div class="d-flex justify-content-end " Style="margin-right:30px;">
			<button class="navbar-toggler" fill="#fff"  type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
			  <span class="navbar-toggler-icon" ></span>
			</button>
			
			<div class="collapse navbar-collapse " id="navbarSupportedContent" >
			  <ul class="navbar-nav me-auto mb-2 mb-lg-0" style="padding-left:10px; ">
				<li class="nav-item-active">
				  <a class="nav-link active" style="color:#fff;" aria-current="page" href="http://localhost/Hack2Hire/index.php">Dashboard</a>
				</li>
				
				<li class="nav-item">
				  <a class="nav-link" style="color:#fff;" href="http://localhost/Hack2Hire/stock.php">Stock Management</a>
				</li>
				
				<li class="nav-item">
				  <a class="nav-link" style="color:#fff;" href="http://localhost/Hack2Hire/report.php">Report</a>
				</li>
			  </ul>
			  
			</div>
		  </div>	
		</div>		  
		</nav>
	<div>
		
	<!--ContentStart-->
	<div >
	
	<?php //if(isset($_GET['error'])){  ?> 
		<div class="alert alert-warning d-flex align-items-between" role="alert" >
		
			<svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" fill="currentColor" class="bi bi-exclamation-triangle" viewBox="0 0 16 16">
			  <path d="M7.938 2.016A.13.13 0 0 1 8.002 2a.13.13 0 0 1 .063.016.146.146 0 0 1 .054.057l6.857 11.667c.036.06.035.124.002.183a.163.163 0 0 1-.054.06.116.116 0 0 1-.066.017H1.146a.115.115 0 0 1-.066-.017.163.163 0 0 1-.054-.06.176.176 0 0 1 .002-.183L7.884 2.073a.147.147 0 0 1 .054-.057zm1.044-.45a1.13 1.13 0 0 0-1.96 0L.165 13.233c-.457.778.091 1.767.98 1.767h13.713c.889 0 1.438-.99.98-1.767L8.982 1.566z"/>
			  <path d="M7.002 12a1 1 0 1 1 2 0 1 1 0 0 1-2 0zM7.1 5.995a.905.905 0 1 1 1.8 0l-.35 3.507a.552.552 0 0 1-1.1 0L7.1 5.995z"/>
			</svg>
			
			<div style="margin: 0px 5px 0px 5px;text-align:right;">
				To show the notification!!!	<?php //echo $_GET['error'];  ?>		
			</div>
			
			
			<button type="button" id="notification_close" style="background-color:#FCF3CF ; border:0px;margin-left:10px;">
			<svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" fill="currentColor" class="bi bi-x-circle" viewBox="0 0 16 16">
			  <path d="M8 15A7 7 0 1 1 8 1a7 7 0 0 1 0 14zm0 1A8 8 0 1 0 8 0a8 8 0 0 0 0 16z"/>
			  <path d="M4.646 4.646a.5.5 0 0 1 .708 0L8 7.293l2.646-2.647a.5.5 0 0 1 .708.708L8.707 8l2.647 2.646a.5.5 0 0 1-.708.708L8 8.707l-2.646 2.647a.5.5 0 0 1-.708-.708L7.293 8 4.646 5.354a.5.5 0 0 1 0-.708z"/>
			</svg>
			</button>
			
		</div>
	<?php // } ?>	
	
	</div>
	
	
	<div class="container">
	  <div class="row align-middle">
		
		<!--LeftSide-->
		<div class="col">
		  <img src="GA_Welcome.png" width="400" height="400" style="margin:50px 0px 0px 60px;">
		</div>
		
		<!--RightSide-->
		<div class="col " >
		  <br><br>
		  <h4 style="text-align:center;">Fantastic Four</h4>
		  <p style="text-align:center; ">--To ease your jobs with us-- <br>  </p>
		  <br><br><br>
		  
			<div class="input-group flex-nowrap">
			  <span class="input-group-text" id="addon-wrapping">
					<svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" fill="currentColor" class="bi bi-envelope" viewBox="0 0 16 16">
					  <path d="M0 4a2 2 0 0 1 2-2h12a2 2 0 0 1 2 2v8a2 2 0 0 1-2 2H2a2 2 0 0 1-2-2V4Zm2-1a1 1 0 0 0-1 1v.217l7 4.2 7-4.2V4a1 1 0 0 0-1-1H2Zm13 2.383-4.708 2.825L15 11.105V5.383Zm-.034 6.876-5.64-3.471L8 9.583l-1.326-.795-5.64 3.47A1 1 0 0 0 2 13h12a1 1 0 0 0 .966-.741ZM1 11.105l4.708-2.897L1 5.383v5.722Z"/>
					</svg>
			  </span>
			  <input type="email" class="form-control" placeholder="Email Address" name="email" id="email" aria-label="email" aria-describedby="addon-wrapping" style="input-focus-border-color: #fff;">
			</div>
			
			<br>
			
			<div class="input-group flex-nowrap" style="weight:30px;">
			  <span class="input-group-text" id="addon-wrapping">
						<svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" fill="currentColor" class="bi bi-shield-lock-fill" viewBox="0 0 16 16">
						<path fill-rule="evenodd" d="M8 0c-.69 0-1.843.265-2.928.56-1.11.3-2.229.655-2.887.87a1.54 1.54 0 0 0-1.044 1.262c-.596 4.477.787 7.795 2.465 9.99a11.777 11.777 0 0 0 2.517 2.453c.386.273.744.482 1.048.625.28.132.581.24.829.24s.548-.108.829-.24a7.159 7.159 0 0 0 1.048-.625 11.775 11.775 0 0 0 2.517-2.453c1.678-2.195 3.061-5.513 2.465-9.99a1.541 1.541 0 0 0-1.044-1.263 62.467 62.467 0 0 0-2.887-.87C9.843.266 8.69 0 8 0zm0 5a1.5 1.5 0 0 1 .5 2.915l.385 1.99a.5.5 0 0 1-.491.595h-.788a.5.5 0 0 1-.49-.595l.384-1.99A1.5 1.5 0 0 1 8 5z"/>
						</svg>
			  </span>
			  <input type="password" class="form-control" placeholder="Password" name="pw" id="pw" aria-label="password" aria-describedby="addon-wrapping" >
			</div>
			
			<br><br>
			<div class="d-md-flex justify-content-md-end">
			<button type="button" class="btn btn-outline-primary" style="margin:20px; color: #fff;" >Go !</button>
			</div>
		</div>
			

		</div>
	 </div>	
	 

</form>
</html>
