-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 17, 2022 at 03:33 PM
-- Server version: 10.4.22-MariaDB
-- PHP Version: 8.1.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `invsystem`
--

-- --------------------------------------------------------

--
-- Table structure for table `inv_update`
--

CREATE TABLE `inv_update` (
  `IU_ID` int(5) NOT NULL,
  `S_ID` int(5) NOT NULL,
  `IU_Date` date NOT NULL,
  `IU_Time` time NOT NULL,
  `IU_TotalAmount` decimal(10,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `inv_update`
--

INSERT INTO `inv_update` (`IU_ID`, `S_ID`, `IU_Date`, `IU_Time`, `IU_TotalAmount`) VALUES
(1, 1, '2022-03-17', '21:18:24', '10000.00');

-- --------------------------------------------------------

--
-- Table structure for table `iu_record`
--

CREATE TABLE `iu_record` (
  `P_ID` int(5) NOT NULL,
  `IU_ID` int(5) NOT NULL,
  `IU_Quantity` int(4) NOT NULL,
  `IU_Remarks` varchar(100) NOT NULL,
  `Supp_Price` decimal(4,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `iu_record`
--

INSERT INTO `iu_record` (`P_ID`, `IU_ID`, `IU_Quantity`, `IU_Remarks`, `Supp_Price`) VALUES
(3, 1, 140, '', '3.80'),
(4, 1, 100, '', '2.90'),
(6, 1, 45, '', '9.00'),
(7, 1, 20, '', '5.50'),
(8, 1, 100, '', '99.99');

-- --------------------------------------------------------

--
-- Table structure for table `iu_result`
--

CREATE TABLE `iu_result` (
  `IU_ID` int(5) NOT NULL,
  `UpdateFlag` tinyint(1) NOT NULL,
  `Remarks` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `iu_result`
--

INSERT INTO `iu_result` (`IU_ID`, `UpdateFlag`, `Remarks`) VALUES
(1, 1, 'Successful insertion');

-- --------------------------------------------------------

--
-- Table structure for table `manufacturer`
--

CREATE TABLE `manufacturer` (
  `M_ID` int(5) NOT NULL,
  `M_Name` varchar(25) NOT NULL,
  `M_Email` varchar(25) NOT NULL,
  `M_ContactNo` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `manufacturer`
--

INSERT INTO `manufacturer` (`M_ID`, `M_Name`, `M_Email`, `M_ContactNo`) VALUES
(1, 'Dettol', 'Malaysia.ConsumerRelation', '1800888889 '),
(2, 'Tan Seng Farmers', 'tsfarmproduce@gmail.com', '0123456789'),
(3, 'Head and Shoulders', 'headnshoulder@gmail.com', '0198765432');

-- --------------------------------------------------------

--
-- Table structure for table `product`
--

CREATE TABLE `product` (
  `P_ID` int(5) NOT NULL,
  `PC_ID` int(5) DEFAULT NULL,
  `S_ID` int(5) NOT NULL,
  `M_ID` int(5) NOT NULL,
  `P_Name` varchar(100) NOT NULL,
  `P_Barcode` varchar(15) NOT NULL,
  `P_Quantity` int(5) NOT NULL,
  `P_Price` decimal(4,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `product`
--

INSERT INTO `product` (`P_ID`, `PC_ID`, `S_ID`, `M_ID`, `P_Name`, `P_Barcode`, `P_Quantity`, `P_Price`) VALUES
(1, 1, 1, 2, 'Napa Cabbage (1 pkg/200g)', '1536894536', 325, '4.10'),
(2, 2, 1, 3, 'Cool Menthol Anti-Dandruff Shampoo 720ml', '1546823436', 125, '27.50'),
(3, 3, 1, 1, 'Brown Antiseptic Liquid 500ml', '2436894545', 140, '28.50'),
(4, 1, 1, 2, 'Tomato (+/-500g)', '15234545435', 100, '3.00'),
(5, 1, 1, 2, 'Broccoli (1pc +/-220g)', '15234542356', 90, '3.50'),
(6, 2, 1, 1, 'Body Soap Cool 100g x 3+1s', '2434564435', 80, '10.00'),
(7, 2, 1, 1, 'Liquid Hand Wash Refill Pouch Sensitive 225ml', '24235242390', 60, '6.00'),
(8, 2, 1, 3, 'Ultra Men Anti-Hairfall Anti-Dandruff Shampoo 315m', '36368945257', 120, '20.00');

-- --------------------------------------------------------

--
-- Table structure for table `prod_cat`
--

CREATE TABLE `prod_cat` (
  `PC_ID` int(4) NOT NULL,
  `PC_Name` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `prod_cat`
--

INSERT INTO `prod_cat` (`PC_ID`, `PC_Name`) VALUES
(1, 'Vegetables'),
(2, 'Toiletries'),
(3, 'Medical Supplies');

-- --------------------------------------------------------

--
-- Table structure for table `prod_return`
--

CREATE TABLE `prod_return` (
  `PR_ID` int(5) NOT NULL,
  `PR_Date` date NOT NULL,
  `PR_Time` time NOT NULL,
  `PR_TotalAmount` decimal(10,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `pr_record`
--

CREATE TABLE `pr_record` (
  `P_ID` int(5) NOT NULL,
  `PR_ID` int(5) NOT NULL,
  `Reason` varchar(100) NOT NULL,
  `PR_Quantity` int(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `supplier`
--

CREATE TABLE `supplier` (
  `S_ID` int(5) NOT NULL,
  `S_Name` varchar(25) NOT NULL,
  `S_Email` varchar(25) NOT NULL,
  `S_ContactNo` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `supplier`
--

INSERT INTO `supplier` (`S_ID`, `S_Name`, `S_Email`, `S_ContactNo`) VALUES
(1, 'Ricky Distributing', 'rickysemail@gmail.com', '0167345923');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `inv_update`
--
ALTER TABLE `inv_update`
  ADD PRIMARY KEY (`IU_ID`),
  ADD KEY `invupdate_fk_1` (`S_ID`);

--
-- Indexes for table `iu_record`
--
ALTER TABLE `iu_record`
  ADD KEY `iurecord_fk_1` (`IU_ID`),
  ADD KEY `iurecord_fk_2` (`P_ID`);

--
-- Indexes for table `iu_result`
--
ALTER TABLE `iu_result`
  ADD KEY `iuresult_fk_1` (`IU_ID`);

--
-- Indexes for table `manufacturer`
--
ALTER TABLE `manufacturer`
  ADD PRIMARY KEY (`M_ID`);

--
-- Indexes for table `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`P_ID`),
  ADD KEY `product_fk_1` (`PC_ID`),
  ADD KEY `product_fk_2` (`S_ID`),
  ADD KEY `product_fk_3` (`M_ID`);

--
-- Indexes for table `prod_cat`
--
ALTER TABLE `prod_cat`
  ADD PRIMARY KEY (`PC_ID`);

--
-- Indexes for table `prod_return`
--
ALTER TABLE `prod_return`
  ADD PRIMARY KEY (`PR_ID`);

--
-- Indexes for table `pr_record`
--
ALTER TABLE `pr_record`
  ADD KEY `prrecord_fk_1` (`PR_ID`),
  ADD KEY `prrecord_fk_2` (`P_ID`);

--
-- Indexes for table `supplier`
--
ALTER TABLE `supplier`
  ADD PRIMARY KEY (`S_ID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `inv_update`
--
ALTER TABLE `inv_update`
  MODIFY `IU_ID` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `manufacturer`
--
ALTER TABLE `manufacturer`
  MODIFY `M_ID` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `product`
--
ALTER TABLE `product`
  MODIFY `P_ID` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `prod_cat`
--
ALTER TABLE `prod_cat`
  MODIFY `PC_ID` int(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `prod_return`
--
ALTER TABLE `prod_return`
  MODIFY `PR_ID` int(5) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `supplier`
--
ALTER TABLE `supplier`
  MODIFY `S_ID` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `inv_update`
--
ALTER TABLE `inv_update`
  ADD CONSTRAINT `invupdate_fk_1` FOREIGN KEY (`S_ID`) REFERENCES `supplier` (`S_ID`);

--
-- Constraints for table `iu_record`
--
ALTER TABLE `iu_record`
  ADD CONSTRAINT `iurecord_fk_1` FOREIGN KEY (`IU_ID`) REFERENCES `inv_update` (`IU_ID`),
  ADD CONSTRAINT `iurecord_fk_2` FOREIGN KEY (`P_ID`) REFERENCES `product` (`P_ID`);

--
-- Constraints for table `iu_result`
--
ALTER TABLE `iu_result`
  ADD CONSTRAINT `iuresult_fk_1` FOREIGN KEY (`IU_ID`) REFERENCES `inv_update` (`IU_ID`);

--
-- Constraints for table `product`
--
ALTER TABLE `product`
  ADD CONSTRAINT `product_fk_1` FOREIGN KEY (`PC_ID`) REFERENCES `prod_cat` (`PC_ID`),
  ADD CONSTRAINT `product_fk_2` FOREIGN KEY (`S_ID`) REFERENCES `supplier` (`S_ID`),
  ADD CONSTRAINT `product_fk_3` FOREIGN KEY (`M_ID`) REFERENCES `manufacturer` (`M_ID`);

--
-- Constraints for table `pr_record`
--
ALTER TABLE `pr_record`
  ADD CONSTRAINT `prrecord_fk_1` FOREIGN KEY (`PR_ID`) REFERENCES `prod_return` (`PR_ID`),
  ADD CONSTRAINT `prrecord_fk_2` FOREIGN KEY (`P_ID`) REFERENCES `product` (`P_ID`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
