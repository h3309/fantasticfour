<html>
<head>
	<title>Gmail Email Inbox using PHP with IMAP</title>
</head>

<body>
	<form>
		<div class="container">
			<?php
				include ("connection.php");     
    
				$inbox = imap_search($connection, 'SUBJECT "alert"');
	
				if (!empty($inbox)) {
            ?>
		<table>
			<?php
				foreach ($inbox as $email) {
                    $overview = imap_fetch_overview($connection, $email, 0);
					$body = imap_fetchbody($connection, $email, '2');
					//$messageExcerpt = substr($message, 0, 150);
					//$partialMessage = trim(quoted_printable_decode($messageExcerpt)); 
					$date = date("d F, Y", strtotime($overview[0]->date));
            ?>
        <tr>
            <td>
                <?php echo $overview[0]->from; ?>
			</td>
            <td>
                <?php echo $overview[0]->subject; ?> - <?php echo $body; ?>
            </td>
			<td>
                <?php echo $date; ?>
            </td>
        </tr>
			<?php
				} // end foreach
			?>
		</table>
			<?php
				} // end if
        
				imap_close($connection);
    
			?>
		</div>
	</form>
</body>